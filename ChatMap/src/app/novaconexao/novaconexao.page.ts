import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-novaconexao',
  templateUrl: './novaconexao.page.html',
  styleUrls: ['./novaconexao.page.scss'],
})
export class NovaconexaoPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*

import { Component, AfterViewInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController } from "@ionic/angular";

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation, // Adicionado
  MyLocation // Adicionado
} from "@ionic-native/google-maps"; //errorr

@Component({
  selector: 'app-novaconexao',
  templateUrl: './novaconexao.page.html',
  styleUrls: ['./novaconexao.page.scss'],
})
export class NovaconexaoPage implements AfterViewInit {
  map: GoogleMap;
  loading: any;

  constructor(public googleMaps: GoogleMaps,
    public plt: Platform,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController) {
    console.log('Construtor');
  }

  ngAfterViewInit() {
    this.plt.ready().then(() => {
      this.initMap();
    });
  }

  initMap() {
    console.log('InitMap Iniciando');
    this.map = this.googleMaps.create('map');
    this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
      let coordinates: LatLng =
        new LatLng(-2.549531, -44.240750);
      let position = {
        target: coordinates,
        zoom: 17
      };
      this.map.animateCamera(position);
      let markerOptions: MarkerOptions = {
        position: coordinates,
        icon: "assets/images/icons8-Marker-64.png",
        title: 'CEST',
        snippet: 'http://www.cest.edu.br/',
        animation: GoogleMapsAnimation.DROP
      };

      const marker = this.map.addMarker(markerOptions).
        then((marker: Marker) => {
          marker.showInfoWindow();
        });

    })
  }

  async onGPSClick() {
    this.map.clear(); // Limpa o mapa

    this.loading = await this.loadingCtrl.create({
      message: 'Aguarde...'
    });

    // Exibe a mensagem de carregamento
    await this.loading.present();

    // Local do dispositivo
    this.map.getMyLocation().then((location: MyLocation) => {
      this.loading.dismiss();
      console.log(JSON.stringify(location, null, 2));

      // Move o mapa para o local determinado
      this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        tilt: 30
      });

      //Cria um marcador no mapa
      let marker: Marker = this.map.addMarkerSync({
        title: 'Eu estou aqui',
        snippet: 'Um subtítulo',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      // exibe o quadro de informacoes
      marker.showInfoWindow();

      // se for clicadom exibe uma mensagem push
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicou!');
      });
    }).catch(err => {
        //Em caso de erro - fecha a tela de carregamento
        this.loading.dismiss();
        //Exibe o push na tela - sobrepondo o app
        this.showToast(err.error_message);
    });
  }

  //Funcao de mensagem push
  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }
}
*/
