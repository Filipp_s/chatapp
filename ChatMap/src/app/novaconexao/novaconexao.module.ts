import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NovaconexaoPageRoutingModule } from './novaconexao-routing.module';

import { NovaconexaoPage } from './novaconexao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NovaconexaoPageRoutingModule
  ],
  declarations: [NovaconexaoPage]
})
export class NovaconexaoPageModule {}
