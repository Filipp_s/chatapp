import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConexoesPage } from './conexoes.page';

describe('ConexoesPage', () => {
  let component: ConexoesPage;
  let fixture: ComponentFixture<ConexoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConexoesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConexoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
